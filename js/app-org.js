'use strict'

var network_pdu = null; // has to use var for hoisting, because other outside function using

const MESH_PROXY_SERVICE = '00001828-0000-1000-8000-00805f9b34fb';
const MEHS_PROXY_SERVICE_SHORT = 0X1828;
const MESH_PROXY_DATA_IN = '00002add-0000-1000-8000-00805f9b34fb';
const MESH_PROXY_DATA_OUT = '00002ade-0000-1000-8000-00805f9b34fb';
const connected = 'connected';
const connecting = 'connecting';
const disconnected = 'disconnect';
const htmlIdBtnScan = 'btn_scan';
const htmlIdBtnConnect = 'btn_connect'
const htmlIdBtnService = 'btn_service';
const htmlIdBtnSubmit = 'btn_submit';
const regCheckHex32 = /^[0-9A-Fa-f]{32}$/; //regex literal
const regCheckInt8 = /^[0-9]{8}$/;
const regCheckTtl = /^([0-9A-Fa-f]|[0-7][0-9A-Fa-f])$/;
const regCheckSrc = /^[0-9A-Ea-e][0-9A-Fa-f]{3}$/; //0x0001-EFFF
const regCheckDst = /^[0-9A-Ea-e][0-9A-Fa-f]{3}$/;
const regCheckAccessPayload = /^([0-9A-Fa-f][0-9A-Fa-f])+$/;

const app = {
    htmlMsg : null,
    accessPayLoadHidden: false, 
    netKey : "7dd7364cd842ad18c17c2b820c84c3d6",
    appKey : "63964771734fbd76e3b40519d1d94a48",
    nid : "",
    aid : "",
    hexEncryKey : "",
    hexPrivacyKey : "",
    networkId : "",
    seq : "0x07080a",
    ivi : 0,
    ivIndex : "12345677",
    selectedDev : null,
    mtu : 33,
    connectedServer : null,    
    htmlBtnScan : null,
    htmlBtnConnect : null,
    htmlBtnSubmit : null,
    htmlBtnService : null,
    htmlInputAccessPayload : null,
    htmlInputTid : null,
    htmlInputTransTime : null,
    htmlInputDelay : null,
    htmlInputNetkey : null,
    htmlInputAppkey : null,
    htmlInputIvIndex : null,
    htmlInputTtl : null,
    htmlInputSrc : null,
    htmlInputDst : null,
    htmlInputMtu : null,
    htmlNetMic : null,
    htmlNetworkPduHex: null,
    htmlHdgNetworkPdu: null,
    htmlHdgProxyPdu : null,
    htmlProxyPduHex: null,    
    htmlSelectedSar : null,
    htmlSelectMsgType : null,
    htmlSelectOpcode : null,
    htmlSelectOnoff : null,
    htmlTransmic: null,
    validPdu: false,
    accessPayload: null,
    srcAddr: "1234",
    dstAddr: "C105",
    netMic:0,
    proxyPdu:"",
    sar:0,    
    ttl : 3,
    ctl : 0,
    A : null,
    state: {
        scanStarted : false,
        connectState : "",
        hasProxyDataInChar : false,
        hasProxyDataOutChar : false,        
    },
    proxyDataInChar : null,
    proxyDataOutChar : null
};

app.calKey = ()=> {
    let k2_material = crypto.k2(app.netKey,"00");    
    app.hexEncryKey = k2_material.encryption_key;
    app.hexPrivacyKey = k2_material.privacy_key;
    app.networkId = crypto.k3(app.netKey);
    app.nid = k2_material.NID;
    app.A = utils.normaliseHex(app.appKey);
    app.aid = crypto.k4(app.appKey);    
};

app.deriveProxyPdu = ()=>{
    const seg = 0;
    const akf = 1;

    app.validPdu = true;
    //access payload
    app.deriveAccessPayload();    
    //upper transport PDU
    // derive Application Nonce (ref 3.8.5.2)    
    let appNonce="0100" + utils.toHex(app.seq, 3) + app.srcAddr + app.dstAddr + app.htmlInputIvIndex.value;
    let upperTransPdu = crypto.meshAuthEncAccessPayload(app.A, appNonce, app.accessPayload);    
    app.htmlTransmic.innerHTML = "0x" + upperTransPdu.TransMIC;
    
    //derive lower transport PDU
    // seg=0 (1 bit), akf=1 (1 bit), aid (6 bits) already derived from k4
    let segInt = parseInt(seg,16);
    let akfInt = parseInt(akf,16);
    let aidInt = parseInt(app.aid,16);
    let ltpdu1 = (segInt << 7) | (akfInt << 6) | aidInt;
    let lowerTransPdu = utils.intToHex(ltpdu1) + upperTransPdu.EncAccessPayload + upperTransPdu.TransMIC;    

    //encrypt network PDU
    let hexDst = app.htmlInputDst.value;
    let ctlInt = parseInt(app.ctl , 16);
    let ttlInt = parseInt(app.ttl,16);
    let ctlttl = (ctlInt | ttlInt);
    let npdu2 = utils.intToHex(ctlttl);
    let N = utils.normaliseHex(app.hexEncryKey);
    let netNonce = "00" + npdu2 + utils.toHex(app.seq,3) + app.srcAddr + "0000" + app.htmlInputIvIndex.value;
    let secureNetworkPdu = crypto.meshAuthEncNetwork(N, netNonce, hexDst, lowerTransPdu);    
    app.netMic = secureNetworkPdu.NetMIC;
    app.htmlNetMic.innerHTML = "0x" + secureNetworkPdu.NetMIC;

    //Obfuscate
    network_pdu = secureNetworkPdu;
    let obfuscatedPdu = app.obfuscateNetworkPdu(secureNetworkPdu);  

    //finalise network PDU
    let I = utils.normaliseHex(app.htmlInputIvIndex.value);
    app.ivi = utils.leastSignificantBit(parseInt(I, 16));    
    let iviInt = parseInt(app.ivi,16);
    let nidInt = parseInt(app.nid,16);
    let npdu1 = utils.intToHex((iviInt << 7) | nidInt);
    let finalisedNetworkPdu = npdu1 + obfuscatedPdu.obfuscated_ctl_ttl_seq_src + secureNetworkPdu.EncDST + secureNetworkPdu.EncTransportPDU + secureNetworkPdu.NetMIC;    
    app.htmlHdgNetworkPdu.innerHTML = "Network PDU - " + (finalisedNetworkPdu.length/2) + " octets";
    app.htmlNetworkPduHex.innerHTML = "0x" + finalisedNetworkPdu;

    //finalise proxy PDU    
    let sm = (app.sar << 6) | app.htmlSelectMsgType.selectedIndex;
    app.proxyPdu = "";
    app.proxyPdu = app.proxyPdu + utils.intToHex(sm);    
    app.proxyPdu = app.proxyPdu + finalisedNetworkPdu;
    console.log("Finalized proxy PDU is :" + app.proxyPdu);
    app.htmlHdgProxyPdu.innerHTML = "Proxy PDU - " + (app.proxyPdu.length /2) + " octets";
    app.htmlProxyPduHex.innerHTML = "0x" +app.proxyPdu;    
    if(app.proxyPdu.length > (app.mtu *2)){
        app.showMessageRed("Segmentation required ( PDU length > MTU)");
        app.validPdu =false;
        app.setBtnState('btn_submit',0);
    }else{
        app.showMessage("Proxy PDU ready");
    }
    app.updateHTML();
};

app.obfuscateNetworkPdu = (network_pdu) => crypto.obfuscate(network_pdu.EncDST, network_pdu.EncTransportPDU, network_pdu.NetMIC, app.ctl, app.ttl, utils.toHex(app.seq, 3), app.srcAddr, app.htmlInputIvIndex.value, app.hexPrivacyKey);

app.deriveAccessPayload = ()=>{
    app.accessPayload = null;
    if(app.htmlSelectOpcode.value==="0000"){
        app.accessPayload = app.htmlInputAccessPayload.value;        
    }else{
        app.accessPayload = app.htmlSelectOpcode.value;
        if(app.accessPayload === "8202" || app.accessPayload === "8203"){
            app.accessPayload = app.accessPayload + app.htmlSelectOnoff.value + app.htmlInputTid.value;
            let tt = document.getElementById("trans_time_hex").value;
            if(tt!=="00"){
                app.accessPayload = app.accessPayload + tt + document.getElementById("delay_hex").value;
            }
        }
    }
};

app.updateHTML = ()=> {
    document.getElementById("nid").innerHTML = "0x" + app.nid;    
    document.getElementById("aid").innerHTML = "0x" + app.aid;
    document.getElementById("encryption_key").innerHTML = "0x" + app.hexEncryKey;
    document.getElementById("privacy_key").innerHTML = "0x" + app.hexPrivacyKey;
    document.getElementById("network_id").innerHTML = "0x" + app.networkId;
    document.getElementById("seq").innerHTML = app.seq;
    document.getElementById("ivi").innerHTML = "0x" + app.ivi.toString();
    document.getElementById("access_payload_section").hidden = app.accessPayloadHidden;
    document.getElementById("generic_onoff_params_onoff").hidden = !app.accessPayloadHidden;
    document.getElementById("generic_onoff_params_tid").hidden = !app.accessPayloadHidden;
    document.getElementById("generic_onoff_params_trans_time").hidden = !app.accessPayloadHidden;
    document.getElementById("generic_onoff_params_delay").hidden = !app.accessPayloadHidden;
};

app.init = ()=>{    
    app.bindElement();            
    app.calKey();    
    app.resetState();        
}

app.resetState = ()=>{    
    app.selectedDev = null;
    app.state.scanStarted=false;
    app.state.connectState=disconnected;
    app.state.hasProxyDataInChar=false;
    app.state.hasProxyDataOutChar=false;
    app.proxyDataInChar=null;
    app.proxyDataOutChar=null;
    app.setButtons();
    app.deriveProxyPdu();
    app.showMessage('Ready');    
}


app.setButtons = ()=>{

    if(app.state.scanStarted===false && app.selectedDev===null && app.state.connectState === disconnected && app.state.hasProxyDataInChar === false)
    {
        app.setBtnGroupState(1,0,0,0);
        app.htmlBtnConnect.textContent="Connect";
    }
    if(app.state.connectState===connecting){
        app.setBtnGroupState(0,0,0,0);
        app.htmlBtnConnect.textContent="Connecting";
    }
    if (app.state.scanStarted===false && app.selectedDev!==null && app.state.connectState === disconnected && app.state.hasProxyDataInChar === false) {
        app.setBtnGroupState(1,1,0,0);
    }
    if (app.state.scanStarted===false && app.selectedDev!==null && app.state.connectState === connected && app.state.hasProxyDataInChar === false) {
        app.setBtnGroupState(0,1,1,0);
        app.htmlBtnConnect.textContent="Discon";
    }
    if (app.state.scanStarted===false && app.selectedDev!==null && app.state.connectState === connected && app.state.hasProxyDataInChar === true) {
        app.setBtnGroupState(0,0,0,1);
    }
}

app.setBtnGroupState = (scan,connect,service,submit)=>{
    app.setBtnState(htmlIdBtnScan,scan);
    app.setBtnState(htmlIdBtnConnect,connect);
    app.setBtnState(htmlIdBtnService,service);
    app.setBtnState(htmlIdBtnSubmit,submit);
}

app.setBtnState = (btnId,state)=>{    
    let btn= document.getElementById(btnId);
    if(state===0){
        btn.disabled=true;
        btn.style.color="gray";
    }else{
        btn.disabled=false;
        btn.style.color="white";
    }
}

app.bindElement=()=>{
    app.htmlBtnScan = document.getElementById('btn_scan');
    app.htmlBtnConnect = document.getElementById('btn_connect');
    app.htmlBtnSubmit = document.getElementById('btn_submit');
    app.htmlBtnService = document.getElementById('btn_service');
    app.htmlHdgNetworkPdu = document.getElementById('hdg_network_pdu');
    app.htmlHdgProxyPdu = document.getElementById('hdg_proxy_pdu');
    app.htmlInputAccessPayload = document.getElementById('access_payload_hex');
    app.htmlInputTid = document.getElementById('tid_hex');
    app.htmlInputTransTime = document.getElementById('trans_time_hex');
    app.htmlInputDelay = document.getElementById('delay_hex');
    app.htmlInputNetkey = document.getElementById('netkey');
    app.htmlInputAppkey = document.getElementById('appkey');
    app.htmlInputIvIndex = document.getElementById('iv_index');
    app.htmlInputTtl = document.getElementById('ttl');
    app.htmlInputSrc = document.getElementById('src');
    app.htmlInputDst = document.getElementById('dst');
    app.htmlInputMtu = document.getElementById('mtu');
    app.htmlNetMic = document.getElementById('net_mic');
    app.htmlNetworkPduHex = document.getElementById('network_pdu_hex');        
    app.htmlMsg = document.getElementById('message');
    app.htmlProxyPduHex = document.getElementById('proxy_pdu_hex');
    app.htmlSelectedSar = document.getElementById('sar_selection');
    app.htmlSelectMsgType = document.getElementById('msg_type');
    app.htmlSelectOpcode = document.getElementById('opcode_selection');
    app.htmlSelectOnoff = document.getElementById('onoff_selection');    
    app.htmlTransmic = document.getElementById('trans_mic');

    app.htmlBtnScan.addEventListener('click',app.startScan);    
    app.htmlBtnConnect.addEventListener('click',app.btnConnectPressed);
    app.htmlBtnSubmit.addEventListener('click',app.submitPdu);
    app.htmlBtnService.addEventListener('click',app.discoverSvrAndChar)
    app.htmlInputAccessPayload.addEventListener('input',app.onAccessPayloadChange);
    app.htmlInputTid.addEventListener('input',app.onTidChange);
    app.htmlInputTransTime.addEventListener('input',app.onTransTimeChange);
    app.htmlInputDelay.addEventListener('input',app.onDelayChange);
    app.htmlInputNetkey.addEventListener('input',app.onNetkeyChange);
    app.htmlInputAppkey.addEventListener('input',app.onAppkeyChange);
    app.htmlInputIvIndex.addEventListener('input',app.onIvIndexChange);
    app.htmlInputTtl.addEventListener('input',app.onTtlChange);
    app.htmlInputSrc.addEventListener('input',app.onSrcChange);
    app.htmlInputDst.addEventListener('input',app.onDstChange);
    app.htmlInputMtu.addEventListener('input',app.onMtuChange);
    app.htmlSelectedSar.addEventListener('change',app.onSarSelect);
    app.htmlSelectMsgType.addEventListener('change',app.onMsgTypeSelect);
    app.htmlSelectOpcode.addEventListener('change',app.onOpcodeSelect);
    app.htmlSelectOnoff.addEventListener('change',app.onOnoffSelect);
}

app.startScan= ()=>{
    console.log('startScan');
    app.showMessage('Start scan');
    app.startScan = true;
    app.state.connectState = disconnected;    
    app.state.hasProxyDataInChar =false;
    app.state.hasProxyDataOutChar=false;
    app.selectedDev = null;

    let options = {
        filters: [{
            services: [MEHS_PROXY_SERVICE_SHORT]
        }]
    }
    navigator.bluetooth.requestDevice(options)
        .then(device => {
            console.log('> Name: ' + device.name + ' ID: ' + device.id + ' Connected: ' + device.gatt.connected);
            app.selectedDev = device;                        
            app.state.scanStarted=false;
            app.state.connectState=disconnected;
            app.state.hasProxyDataInChar=false;
            app.state.hasProxyDataOutChar=false;
            app.showMessage(device.name +' selected, ready to connect');
            app.setButtons();
        })
        .catch(error => {
            app.showMessageRed(error);
            console.log('ERROR: ' + error);
            app.resetState();
        });    
};

app.btnConnectPressed=()=>{
    console.log('Connect button pressed');
    if(app.state.connectState===disconnected){
        app.connect();
    }else{
        app.disconnect();
    }
}

app.connect=()=>{
    console.log('Connecting');
    app.state.connectState=connecting;
    app.setButtons();
    app.showMessage('Connecting');
    app.selectedDev.gatt.connect()
        .then(
            (server)=>{
                console.log("Connected to " + server.device.id);
                app.showMessage("Connected to " + server.device.id);                
                app.connectedServer = server;
                app.selectedDev.addEventListener('gattserverdisconnected', app.onDisconnected);
                app.state.scanStarted=false;
                app.state.connectState=connected;
                app.state.hasProxyDataInChar=false;
                app.state.hasProxyDataOutChar=false;
                app.setButtons();
            },
            (err)=>{
                console.log("ERROR: could not connect - " + err);
                app.showMessageRed("ERROR: could not connect - " + err);
                app.resetState();
            }
        );        
}
app.disconnect=()=>{
    console.log('Disconnecting');
    app.connectedServer.disconnect();
    app.resetState();          
};

app.onDisconnected = ()=>{
    console.log("onDisconnected");
    app.resetState(); 
}



app.discoverSvrAndChar=async()=>{
    let services = await app.connectedServer.getPrimaryServices();
    services.forEach(async (service,svIdx,svArray)=>{
        if(service.uuid === MESH_PROXY_SERVICE){
            console.log('MESH_PROXY_SERVICE '+MESH_PROXY_SERVICE);
            console.log('service.uuid '+service.uuid);            
            let characteristics = await service.getCharacteristics();
            characteristics.forEach((characteristic,chIdx,chArray)=>{
                if(characteristic.uuid === MESH_PROXY_DATA_IN){
                    console.log("MESH_PROXY_DATA_IN :"+MESH_PROXY_DATA_IN);
                    console.log("characteristic :"+characteristic.uuid);
                    app.proxyDataInChar=characteristic;
                    app.state.hasProxyDataInChar = true;
                    app.setButtons();          
                }
                if(characteristic.uuid === MESH_PROXY_DATA_OUT){
                    console.log("MESH_PROXY_DATA_OUT :"+MESH_PROXY_DATA_OUT);
                    console.log("characteristic :"+characteristic.uuid);
                    app.proxyDataOutChar=characteristic;               
                    app.state.hasProxyDataOutChar = true;                    
                }
                if((svIdx === svArray.length - 1) && (chIdx === chArray.length - 1)){
                    console.log("svIdx :" + svIdx + " chIdx :" + chIdx);
                    console.log("last characteristic discovered");
                }                
            });                        
        }
    });
}


app.showMessageRed = (msg_text)=> {
    app.htmlMsg.style.color = "#ff0000";
    app.htmlMsg.innerHTML = msg_text;
    app.htmlMsg.hidden = false;
};

app.showMessage = (msg_text)=> {    
    app.htmlMsg.style.color = 'black';
    app.htmlMsg.innerHTML = msg_text;
    app.htmlMsg.hidden = false;
};

app.submitPdu = async () =>{
    let proxyPduBytes = utils.hexToBytes(app.proxyPdu);
    let proxyPduData = new Uint8Array(proxyPduBytes);
    try{
        await app.proxyDataInChar.writeValue(proxyPduData.buffer);
    }catch(e){
        app.showMessageRed('Error: ' + error);
        console.log('Error: ' + error);
    }    
}

app.onAccessPayloadChange=(e)=>{
    console.log("regCheckAccessPayload :"+regCheckAccessPayload.test(e.target.value));
    if(regCheckAccessPayload.test(e.target.value)){        
        app.accessPayload = e.target.value;
        app.deriveProxyPdu();
    }else{
        app.showMessageRed("Access Payload invalid");
        app.validPdu = false;    
    }
    
}
app.onTidChange=(e)=>{
    console.log('onTidChange:'+e.target.value);
}
app.onTransTimeChange=(e)=>{
    console.log('onTranstimeChange:'+e.target.value);
}
app.onDelayChange=(e)=>{
    console.log('onDelayChange:'+e.target.value);
}

app.onNetkeyChange=(e)=>{ 
    if(regCheckHex32.test(e.target.value)){
        app.netKey = e.target.value;
        app.calKey();
        app.deriveProxyPdu();  
    }else{
        app.showMessageRed("Net key invalid");
        app.validPdu = false;    
    }     
}

app.onAppkeyChange=(e)=>{
    if(regCheckHex32.test(e.target.value)){        
        app.appKey =  e.target.value;
        app.calKey();        
        app.deriveProxyPdu();
    }else{        
        app.showMessageRed("App key invalid");
        app.validPdu = false;
    }    
}
app.onIvIndexChange=(e)=>{    
    if(regCheckInt8.test(e.target.value)){        
        app.deriveProxyPdu();
    }else{
        app.showMessageRed("Iv Index invalid");
        app.validPdu = false;
    }    
}
app.onTtlChange=(e)=>{        
    if(regCheckTtl.test(e.target.value)){
        app.ttl=e.target.value;
        app.deriveProxyPdu();
    }else{
        app.showMessageRed("TTL value invalid");
        app.validPdu = false;
    }    
}
app.onSrcChange=(e)=>{
    if(regCheckSrc.test(e.target.value)){
        app.srcAddr = e.target.value;
        app.deriveProxyPdu();
    }else{
        app.showMessageRed("SRC value invalid");
        app.validPdu = false;
    }    
}
app.onDstChange=(e)=>{
    if(regCheckDst.test(e.target.value)){
        app.dstAddr = e.target.value;
        app.deriveProxyPdu();
    }else{
        app.showMessageRed("DST value invalid");
        app.validPdu = false;
    } 
}
app.onMtuChange=(e)=>{
    app.mtu = parseInt(e.target.value);    
    if(isNaN(app.mtu)){        
        app.showMessageRed("MTU value empty");        
    }else{
        app.calKey();
        app.deriveProxyPdu();
    }
}
app.onSarSelect=(e)=>{
    app.sar = parseInt(e.target.value);
    app.calKey();
    app.deriveProxyPdu();
}
app.onMsgTypeSelect=(e)=>{
    console.log('onMsgTypeSelect:'+e.target.value);
    app.calKey();
    app.deriveProxyPdu();
}

app.onOpcodeSelect=(e)=>{    
    if(e.target.value === "0000"){
        app.accessPayloadHidden = false;
    }else{
        app.accessPayloadHidden = true;
    }      
    app.updateHTML();
}
app.onOnoffSelect=(e)=>{
    console.log('onOnoffSelect:'+e.target.value);
}