'use strict'

var network_pdu = null; // has to use var for hoisting, because other outside function using


const NETKEY_DEFAULT= '7dd7364cd842ad18c17c2b820c84c3d6';
const APPKEY_DEFAULT= '63964771734fbd76e3b40519d1d94a48';
const MESH_PROXY_SERVICE = '00001828-0000-1000-8000-00805f9b34fb';
const MEHS_PROXY_SERVICE_SHORT = 0X1828;
const MESH_PROXY_DATA_IN = '00002add-0000-1000-8000-00805f9b34fb';
const MESH_PROXY_DATA_OUT = '00002ade-0000-1000-8000-00805f9b34fb';
const connected = 'connected';
const connecting = 'connecting';
const disconnected = 'disconnect';
const htmlIdBtnScan = 'btn_scan';
const htmlIdBtnConnect = 'btn_connect'
const htmlIdBtnService = 'btn_service';
const htmlIdBtnSubmit = 'btn_submit';
const regCheckHex32 = /^[0-9A-Fa-f]{32}$/; //regex literal
const regCheckInt8 = /^[0-9]{8}$/;
const regCheckTtl = /^([0-9A-Fa-f]|[0-7][0-9A-Fa-f])$/;
const regCheckSrc = /^[0-9A-Ea-e][0-9A-Fa-f]{3}$/;
const regCheckDst = /^[0-9A-Fa-f]{4}$/;
const regCheckAccessPayload = /^([0-9A-Fa-f][0-9A-Fa-f])+$/;

const app = {
    htmlMsg: null,
    accessPayloadShow: null,
    netKey: null,
    appKey: null,
    nid: null,
    aid: null,
    hexEncryKey: null,
    hexPrivacyKey: null,
    networkID: null,
    seq: null,
    ivi: null,
    ivIndex: null,
    selectedDev: null,
    mtu: null,
    connectedServer: null,
    htmlAid: null,
    htmlBtnConnect: null,
    htmlIvi: null,
    htmlNid: null,
    htmlNetMic: null,
    htmlNetworkPDUHex: null,
    htmlNetworkID: null,
    htmlNetworkPDULength: null,
    htmlProxyPDULength: null,
    htmlProxyPDUHex: null,
    htmlSEQ: null,
    htmlTransmic: null,
    htmlEncryKey: null,
    htmlPrivacyKey: null,
    validPdu: null,
    rawAccessPayload: null,
    finalAccessPayload: null,
    srcAddr: null,
    dstAddr: null,
    proxyPdu: null,
    selectedMsgType: null,
    selectedOpcode: null,
    selectedOnoffValue: null,
    sar: null,
    ttl: null,
    tid: null,
    ctl: null,
    A: null,
    state: {
        scanStarted: null,
        connectState: null,
        hasProxyDataInChar: null,
        hasProxyDataOutChar: null,
    },
    proxyDataInChar: null,
    proxyDataOutChar: null
};

let initData = () => {
    app.accessPayloadShow = true;
    app.netKey = NETKEY_DEFAULT;
    app.appKey = APPKEY_DEFAULT;
    app.rawAccessPayload = "d50a0048656c6c6f";
    app.seq = 460810;// 0x0x07080a
    app.ivIndex = "12345677";
    app.mtu = 33;
    app.validPdu = false;
    app.srcAddr = "1234";
    app.dstAddr = "C105";
    app.sar = 0;
    app.ttl = 3;
    app.ctl = 0;
    app.state.scanStarted = false;
    app.state.hasProxyDataInChar = false;
    app.state.hasProxyDataOutChar = false;
    app.selectedOpcode = "0000"
}

let bindElements = () => {
    //bind elements with default value
    document.getElementById('input-mtu-value').addEventListener('input', app.onMtuChange);
    document.getElementById('input-mtu-value').value = app.mtu;
    document.getElementById('input-netkey-value').addEventListener('input', app.onNetkeyChange);
    document.getElementById('input-netkey-value').value = app.netKey.toUpperCase();
    document.getElementById('input-appkey-value').addEventListener('input', app.onAppkeyChange);
    document.getElementById('input-appkey-value').value = app.appKey.toUpperCase();
    app.htmlSEQ = document.getElementById("output-seq-value");
    app.htmlSEQ.innerHTML = "0x " + utils.toHex(app.seq, 3);
    document.getElementById('input-ivIndex-value').addEventListener('input', app.onIvIndexChange);
    document.getElementById('input-ivIndex-value').value = app.ivIndex;
    document.getElementById('input-src-value').addEventListener('input', app.onSrcChange);
    document.getElementById('input-src-value').value = app.srcAddr;
    document.getElementById('input-dst-value').addEventListener('input', app.onDstChange);
    document.getElementById('input-dst-value').value = app.dstAddr;
    document.getElementById('input-ttl-value').addEventListener('input', app.onTtlChange);
    document.getElementById('input-ttl-value').value = app.ttl;
    document.getElementById('input-sar-value').addEventListener('change', app.onSarSelect);
    document.getElementById('input-sar-value').value = app.sar;
    document.getElementById('input-payload-value').addEventListener('input', app.onAccessPayloadChange);
    document.getElementById('input-payload-value').value = app.rawAccessPayload;
    document.getElementById('input-opcode-value').addEventListener('change', app.onOpcodeSelect);
    document.getElementById('input-opcode-value').value = app.selectedOpcode;
    //bind buttons
    document.getElementById('btn_scan').addEventListener('click', app.startScan);
    app.htmlBtnConnect = document.getElementById('btn_connect');
    app.htmlBtnConnect.addEventListener('click', app.btnConnectPressed);
    document.getElementById('btn_submit').addEventListener('click', app.submitPdu);
    document.getElementById('btn_service').addEventListener('click', app.discoverSvrAndChar);


    document.getElementById('input-onoff-tid-value').addEventListener('input', app.onTidChange);
    document.getElementById('input-onoff-transTime-value').addEventListener('input', app.onTransTimeChange);
    document.getElementById('input-onoff-delay-value').addEventListener('input', app.onDelayChange);




    document.getElementById('input-msgType-value').addEventListener('change', app.onMsgTypeSelect);

    document.getElementById('input-onoff-state-value').addEventListener('change', app.onOnoffSelect);

    app.htmlAid = document.getElementById("output-aid-value");
    app.htmlIvi = document.getElementById("output-ivi-value");
    app.htmlNid = document.getElementById("output-nid-value");
    app.htmlNetMic = document.getElementById('output-netMIC-value');
    app.htmlNetworkID = document.getElementById("output-networkID-value")
    app.htmlNetworkPDULength = document.getElementById('output-networkPDU-length');
    app.htmlNetworkPDUHex = document.getElementById('output-networkPDU-value');
    app.htmlProxyPDULength = document.getElementById('output-proxyPDU-length');
    app.htmlProxyPDUHex = document.getElementById('output-proxyPDU-value');
    app.htmlMsg = document.getElementById('message-content');
    app.htmlTransmic = document.getElementById('output-transMIC-value');
    app.htmlEncryKey = document.getElementById('output-encryptionKey-value');
    app.htmlPrivacyKey = document.getElementById('output-privacyKey-value');
}

app.calKey = () => {
    let k2_material = crypto.k2(app.netKey, "00");
    app.hexEncryKey = k2_material.encryption_key;
    app.hexPrivacyKey = k2_material.privacy_key;
    app.networkID = crypto.k3(app.netKey);
    app.nid = k2_material.NID;
    app.A = utils.normaliseHex(app.appKey);
    app.aid = crypto.k4(app.appKey);
};

app.deriveProxyPdu = () => {
    const seg = 0;
    const akf = 1;

    app.validPdu = true;
    //access payload
    app.deriveAccessPayload();
    //upper transport PDU
    // derive Application Nonce (ref 3.8.5.2)    
    let appNonce = "0100" + utils.toHex(app.seq, 3) + app.srcAddr + app.dstAddr + app.ivIndex;
    let upperTransPdu = crypto.meshAuthEncAccessPayload(app.A, appNonce, app.finalAccessPayload);
    app.htmlTransmic.innerHTML = "0x" + upperTransPdu.TransMIC.toUpperCase();

    //derive lower transport PDU
    // seg=0 (1 bit), akf=1 (1 bit), aid (6 bits) already derived from k4
    let segInt = parseInt(seg, 16);
    let akfInt = parseInt(akf, 16);
    let aidInt = parseInt(app.aid, 16);
    let ltpdu1 = (segInt << 7) | (akfInt << 6) | aidInt;
    let lowerTransPdu = utils.intToHex(ltpdu1) + upperTransPdu.EncAccessPayload + upperTransPdu.TransMIC;

    //encrypt network PDU    
    let ctlInt = parseInt(app.ctl, 16);
    let ttlInt = parseInt(app.ttl, 16);
    let ctlttl = (ctlInt | ttlInt);
    let npdu2 = utils.intToHex(ctlttl);
    let N = utils.normaliseHex(app.hexEncryKey);
    let netNonce = "00" + npdu2 + utils.toHex(app.seq, 3) + app.srcAddr + "0000" + app.ivIndex;
    let secureNetworkPdu = crypto.meshAuthEncNetwork(N, netNonce, app.dstAddr, lowerTransPdu);
    app.htmlNetMic.innerHTML = "0x" + secureNetworkPdu.NetMIC.toUpperCase();

    //Obfuscate
    network_pdu = secureNetworkPdu;
    let obfuscatedPdu = app.obfuscateNetworkPdu(secureNetworkPdu);

    //finalise network PDU
    let I = utils.normaliseHex(app.ivIndex);
    app.ivi = utils.leastSignificantBit(parseInt(I, 16));
    let iviInt = parseInt(app.ivi, 16);
    let nidInt = parseInt(app.nid, 16);
    let npdu1 = utils.intToHex((iviInt << 7) | nidInt);
    let finalisedNetworkPdu = npdu1 + obfuscatedPdu.obfuscated_ctl_ttl_seq_src + secureNetworkPdu.EncDST + secureNetworkPdu.EncTransportPDU + secureNetworkPdu.NetMIC;
    app.htmlNetworkPDULength.innerHTML = finalisedNetworkPdu.length / 2;
    app.htmlNetworkPDUHex.innerHTML = "0x " + finalisedNetworkPdu.toUpperCase();

    //finalise proxy PDU    
    let sm = (app.sar << 6) | app.selectedMsgType;
    app.proxyPdu = "";
    app.proxyPdu = app.proxyPdu + utils.intToHex(sm);
    app.proxyPdu = app.proxyPdu + finalisedNetworkPdu;
    console.log("Finalized proxy PDU is :" + app.proxyPdu);
    app.htmlProxyPDULength.innerHTML = app.proxyPdu.length / 2;
    app.htmlProxyPDUHex.innerHTML = "0x " + app.proxyPdu.toUpperCase();
    if (app.proxyPdu.length > (app.mtu * 2)) {
        app.showMessageRed("Segmentation required ( PDU length > MTU)");
        app.validPdu = false;
        app.setBtnState('btn_submit', 0);
    } else {
        app.showMessage("Proxy PDU ready");
    }
    app.updateHTML();
};

app.obfuscateNetworkPdu = (network_pdu) => crypto.obfuscate(network_pdu.EncDST, network_pdu.EncTransportPDU, network_pdu.NetMIC, app.ctl, app.ttl, utils.toHex(app.seq, 3), app.srcAddr, app.ivIndex, app.hexPrivacyKey);

app.deriveAccessPayload = () => {
    app.finalAccessPayload = null;
    if (app.selectedOpcode === "0000") {
        app.finalAccessPayload = app.rawAccessPayload;
    } else {
        app.finalAccessPayload = app.selectedOpcode;
        if (app.finalAccessPayload === "8202" || app.finalAccessPayload === "8203") {
            app.finalAccessPayload = app.finalAccessPayload + app.selectedOnoffValue + app.tid;
            let tt = document.getElementById("input-onoff-transTime-value").value;
            if (tt !== "00") {
                app.finalAccessPayload = app.finalAaccessPayload + tt + document.getElementById("input-onoff-delay-value").value;
            }
        }
    }
};

app.updateHTML = () => {
    app.htmlNid.innerHTML = "0x" + app.nid;
    app.htmlAid.innerHTML = "0x" + app.aid;
    app.htmlEncryKey.innerHTML = "0x " + app.hexEncryKey.toUpperCase();
    app.htmlPrivacyKey.innerHTML = "0x " + app.hexPrivacyKey.toUpperCase();
    app.htmlNetworkID.innerHTML = "0x " + app.networkID.toUpperCase();
    app.htmlSEQ.innerHTML = "0x" + utils.toHex(app.seq, 3);
    app.htmlIvi.innerHTML = "bit: " + app.ivi.toString();
    if(app.accessPayloadShow===true){
        classVisible("custom-payload-input", true);
        classVisible("generic-onoff-input", false);
    }else{
        classVisible("custom-payload-input", false);
        classVisible("generic-onoff-input", true);
    }

};

app.resetState = () => {
    app.selectedDev = null;
    app.state.scanStarted = false;
    app.state.connectState = disconnected;
    app.state.hasProxyDataInChar = false;
    app.state.hasProxyDataOutChar = false;
    app.proxyDataInChar = null;
    app.proxyDataOutChar = null;
    app.setButtons();
    updatePDU();
    app.showMessage('Ready');
}


app.setButtons = () => {
    if(app.state.scanStarted === false){
        if(app.state.connectState === disconnected){
            app.htmlBtnConnect.textContent = "Connect";
            if (app.selectedDev === null)    
            {
                    app.setBtnGroupState(1, 0, 0, 0);                
            }
            if (app.selectedDev !== null){
                    app.setBtnGroupState(1, 1, 0, 0);                   
            }
        }else if(app.state.connectState === connected){
            app.htmlBtnConnect.textContent = "Discon";
            if (app.state.hasProxyDataInChar === false) {
                    app.setBtnGroupState(0, 1, 1, 0);                    
            }    
            if (app.state.hasProxyDataInChar === true) {
                app.setBtnGroupState(0, 1, 0, 1);
            }
        }
    }else if(app.state.connectState === connecting) {
        app.htmlBtnConnect.textContent = "Discon";
        app.setBtnGroupState(0, 1, 0, 0);
        app.htmlBtnConnect.textContent = "Connecting";
    } 
}

app.setBtnGroupState = (scan, connect, service, submit) => {
    app.setBtnState(htmlIdBtnScan, scan);
    app.setBtnState(htmlIdBtnConnect, connect);
    app.setBtnState(htmlIdBtnService, service);
    app.setBtnState(htmlIdBtnSubmit, submit);
}

app.setBtnState = (btnId, state) => {
    let btn = document.getElementById(btnId);
    if (state === 0) {
        btn.disabled = true;
        btn.style.color = "gray";
    } else {
        btn.disabled = false;
        btn.style.color = "white";
    }
}

app.startScan = () => {
    console.log('startScan');
    app.showMessage('Start scan');
    app.startScan = true;
    app.state.connectState = disconnected;
    app.state.hasProxyDataInChar = false;
    app.state.hasProxyDataOutChar = false;
    app.selectedDev = null;

    let options = {
        filters: [{
            services: [MEHS_PROXY_SERVICE_SHORT]
        }]
    }
    navigator.bluetooth.requestDevice(options)
        .then(device => {
            console.log('> Name: ' + device.name + ' ID: ' + device.id + ' Connected: ' + device.gatt.connected);
            app.selectedDev = device;
            app.state.scanStarted = false;
            app.state.connectState = disconnected;
            app.state.hasProxyDataInChar = false;
            app.state.hasProxyDataOutChar = false;
            app.showMessage(device.name + ' selected, ready to connect');
            app.setButtons();
        })
        .catch(error => {
            app.showMessageRed(error);
            console.log('ERROR: ' + error);
            app.resetState();
        });
};

app.btnConnectPressed = () => {
    console.log('Connect button pressed');
    if (app.state.connectState === disconnected) {
        app.connect();
    } else {
        app.disconnect();
    }
}

app.connect = () => {
    console.log('Connecting');
    app.state.connectState = connecting;
    app.setButtons();
    app.showMessage('Connecting');
    app.selectedDev.gatt.connect()
        .then(
            (server) => {
                console.log("Connected to " + server.device.id);
                app.showMessage("Connected to " + server.device.id);
                app.connectedServer = server;
                app.selectedDev.addEventListener('gattserverdisconnected', app.onDisconnected);
                app.state.scanStarted = false;
                app.state.connectState = connected;
                app.state.hasProxyDataInChar = false;
                app.state.hasProxyDataOutChar = false;
                app.setButtons();
            },
            (err) => {
                console.log("ERROR: could not connect - " + err);
                app.showMessageRed("ERROR: could not connect - " + err);
                app.resetState();
            }
        );
}
app.disconnect = () => {
    console.log('Disconnecting');
    app.connectedServer.disconnect();    
};

app.onDisconnected = () => {
    console.log("onDisconnected");
    app.resetState();
}



app.discoverSvrAndChar = async () => {
    let services = await app.connectedServer.getPrimaryServices();
    services.forEach(async (service, svIdx, svArray) => {
        if (service.uuid === MESH_PROXY_SERVICE) {
            console.log('MESH_PROXY_SERVICE ' + MESH_PROXY_SERVICE);
            console.log('service.uuid ' + service.uuid);
            let characteristics = await service.getCharacteristics();
            characteristics.forEach((characteristic, chIdx, chArray) => {
                if (characteristic.uuid === MESH_PROXY_DATA_IN) {
                    console.log("MESH_PROXY_DATA_IN :" + MESH_PROXY_DATA_IN);
                    console.log("characteristic :" + characteristic.uuid);
                    app.proxyDataInChar = characteristic;
                    app.state.hasProxyDataInChar = true;
                    app.setButtons();
                }
                if (characteristic.uuid === MESH_PROXY_DATA_OUT) {
                    console.log("MESH_PROXY_DATA_OUT :" + MESH_PROXY_DATA_OUT);
                    console.log("characteristic :" + characteristic.uuid);
                    app.proxyDataOutChar = characteristic;
                    app.state.hasProxyDataOutChar = true;
                }
                if ((svIdx === svArray.length - 1) && (chIdx === chArray.length - 1)) {
                    console.log("svIdx :" + svIdx + " chIdx :" + chIdx);
                    console.log("last characteristic discovered");
                }
            });            
            if((app.state.hasProxyDataInChar===true)&&(app.state.hasProxyDataOutChar===true)){
                app.showMessage('Data In and Data Out Char Found');
            }else if(app.state.hasProxyDataInChar===true){
                app.showMessage('Only Data In Char Found');
            }else if(app.state.hasProxyDataOutChar===true){
                app.showMessage('Only Data Out Char Found');
            }else{
                app.showMessage('No Mesh Proxy Char Found');
            }
        }
    });
}


app.showMessageRed = (msg_text) => {
    app.htmlMsg.style.color = "#ff0000";
    app.htmlMsg.innerHTML = msg_text;
    app.htmlMsg.hidden = false;
};

app.showMessage = (msg_text) => {
    app.htmlMsg.style.color = 'black';
    app.htmlMsg.innerHTML = msg_text;
    app.htmlMsg.hidden = false;
};

app.submitPdu = async () => {
    let proxyPduBytes = utils.hexToBytes(app.proxyPdu);
    let proxyPduData = new Uint8Array(proxyPduBytes);
    try {
        await app.proxyDataInChar.writeValue(proxyPduData.buffer);
    } catch (e) {
        app.showMessageRed('Error: ' + error);
        console.log('Error: ' + error);
    }
}

app.onAccessPayloadChange = (e) => {
    console.log("regCheckAccessPayload :" + regCheckAccessPayload.test(e.target.value));
    if (regCheckAccessPayload.test(e.target.value)) {
        app.rawAccessPayload = e.target.value;
        app.deriveProxyPdu();
    } else {
        app.showMessageRed("Access Payload invalid");
        app.validPdu = false;
    }

}
app.onTidChange = (e) => {
    console.log('onTidChange:' + e.target.value);
    app.tid = e.target.value;
}
app.onTransTimeChange = (e) => {
    console.log('onTranstimeChange:' + e.target.value);
}

app.onTransDelayChange = (e) => {
    console.log('onDelayChange:' + e.target.value);
}

app.onDelayChange = (e) => {
    console.log('onDelayChange:' + e.target.value);
}

app.onNetkeyChange = (e) => {
    if (regCheckHex32.test(e.target.value)) {
        app.netKey = e.target.value;
        updatePDU();
    } else {
        app.showMessageRed("Net key invalid");
        app.validPdu = false;
    }
}

app.onAppkeyChange = (e) => {
    if (regCheckHex32.test(e.target.value)) {
        app.appKey = e.target.value;
        updatePDU()
    } else {
        app.showMessageRed("App key invalid");
        app.validPdu = false;
    }
}
app.onIvIndexChange = (e) => {
    if (regCheckInt8.test(e.target.value)) {
        app.ivIndex = e.target.value;
        app.deriveProxyPdu();
    } else {
        app.showMessageRed("Iv Index invalid");
        app.validPdu = false;
    }
}
app.onTtlChange = (e) => {
    if (regCheckTtl.test(e.target.value)) {
        app.ttl = e.target.value;
        app.deriveProxyPdu();
    } else {
        app.showMessageRed("TTL value invalid");
        app.validPdu = false;
    }
}
app.onSrcChange = (e) => {
    if (regCheckSrc.test(e.target.value)) {
        app.srcAddr = e.target.value;
        app.deriveProxyPdu();
    } else {
        app.showMessageRed("SRC value invalid");
        app.validPdu = false;
    }
}
app.onDstChange = (e) => {
    if (regCheckDst.test(e.target.value)) {
        app.dstAddr = e.target.value;
        app.deriveProxyPdu();
    } else {
        app.showMessageRed("DST value invalid");
        app.validPdu = false;
    }
}
app.onMtuChange = (e) => {
    app.mtu = parseInt(e.target.value);
    if (isNaN(app.mtu)) {
        app.showMessageRed("MTU value empty");
    } else {
        updatePDU();
    }
}
app.onSarSelect = (e) => {
    app.sar = parseInt(e.target.value);
    updatePDU();
}
app.onMsgTypeSelect = (e) => {
    console.log('onMsgTypeSelect:' + e.target.value);
    app.selectedMsgType = e.target.value;
    updatePDU();
}

app.onOpcodeSelect = (e) => {
    app.selectedOpcode = e.target.value;
    if (app.selectedOpcode === "0000") {
        app.accessPayloadShow = true;
    } else {
        app.accessPayloadShow = false;
    }
    updatePDU();    
}
app.onOnoffSelect = (e) => {
    console.log('onOnoffSelect:' + e.target.value);
    app.selectedOnoffValue = e.target.value;
    updatePDU();
}

let classVisible = (elementClass, visible) => {
    let elements = document.getElementsByClassName(elementClass);
    for (let i = 0; i < elements.length; i++) {
        if (visible === true) {
            elements[i].hidden = false;
        } else {
            elements[i].hidden = true;
        }
    }
}

let updatePDU=()=>{
    app.calKey();
    app.deriveProxyPdu();
}

(() => {
    initData();
    bindElements();    
    app.resetState();
})();
